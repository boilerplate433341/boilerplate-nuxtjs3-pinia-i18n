require('dotenv').config()

const isDevelopment = process.env.NODE_ENV === 'development'

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  ssr: false,
  devtools: { enabled: isDevelopment },
  debug: false,
  css: [
    '~/assets/css/main.css',
    '~/assets/css/typography.css',
    '~/assets/css/alert.css',
    '~/assets/css/avatar.css',
    '~/assets/css/badge.css',
    '~/assets/css/button.css',
    '~/assets/css/layout.css',
    '~/assets/css/transition.css',
  ],
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },
  modules: [
    '@nuxtjs/i18n',
    '@pinia/nuxt'
  ],
  plugins: [],  // automatically import
  i18n: {
    baseUrl: process.env.BASE_URL,
    detectBrowserLanguage: {
      alwaysRedirect: false,
      // cookieCrossOrigin: true,
      // cookieDomain: null,
      // cookieKey: 'locale',
      // cookieSecure: true,
      fallbackLocale: process.env.DEFAULT_LOCALE,
      redirectOn: 'root',
      useCookie: false
    } ,
    defaultLocale: process.env.DEFAULT_LOCALE,
    locales: [
      { code: 'th', iso: 'th-TH' },
      { code: 'en', iso: 'en-US' },
    ],
    strategy: 'prefix',
    trailingSlash: true,
  },
  app: {
    head: {
      link: [
        { rel: 'preconnect', href: 'https://fonts.googleapis.com' },
        { rel: 'preconnect', href: 'https://fonts.gstatic.com', crossorigin: ""  },
        { href: 'https://fonts.googleapis.com/css2?family=Anuphan:wght@400;600;700&family=Inter:wght@400;700&display=swap', rel: 'stylesheet' },
      ],
      meta: [],
    }
  }
})
