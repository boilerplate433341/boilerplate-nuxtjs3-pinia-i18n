export default {
  create: async (name, avatar) => {
    const axios = useAxios()
    return await axios({
      url: '/organizers',
      method: 'POST',
      data: {
        name,
        avatar
      }
    })
  },
  list: async () => {
    const axios = useAxios()
    return await axios({
      url: '/organizers',
      method: 'GET'
    })
  },
}