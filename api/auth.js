export default {
  login: async (email, password) => {
    const axios = useAxios()
    return await axios({
      url: '/test',
      method: 'POST',
      data: {
        email,
        password
      }
    })
  },
  signUp: async () => {},
  me: async () => {
    const axios = useAxios()
    return await axios({
      url: '/me',
      method: 'GET'
    })
  },
}