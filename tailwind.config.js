/** @type {import('tailwindcss').Config} */

const colors = {
  white: '#FFFFFF',
  'neutral-alpha': {
    5: '#17171A0C',
    10: '#17171A19',
    20: '#17171A32',
    30: '#17171A4C',
    40: '#17171A66',
    50: '#17171A7F',
    60: '#17171A99',
    70: '#17171AB2',
    80: '#17171ACC',
  },
  neutral: {
    10: '#F3F3F4',
    20: '#D7D7D9',
    30: '#BBBBBF',
    40: '#9D9DA3',
    50: '#808188',
    60: '#62636B',
    70: '#46464C',
    80: '#2D2E32',
    90: '#17171A'
  },
  brand: {
    10: '#F2F3FD',
    20: '#D1D5F8',
    30: '#B0B8F4',
    40: '#8B98EF',
    50: '#6478E9',
    60: '#3557D9',
    70: '#243D9F',
    80: '#16276D',
    90: '#08133F',
  },
  negative: {
    10: '#FEF1F1',
    20: '#FBCCCC',
    30: '#F9A5A5',
    40: '#F87576',
    50: '#F72F31',
    60: '#C31C1D',
    70: '#8E1113',
    80: '#610809',
    90: '#370303',
  },
  warning: {
    10: '#FEF1E6',
    20: '#FDCF99',
    30: '#F4AF28',
    40: '#C39320',
    50: '#A97819',
    60: '#835C11',
    70: '#5E4109',
    80: '#3F2A04',
    90: '#221501',
  },
  positive: {
    10: '#D2FEE1',
    20: '#46F39B',
    30: '#3CD486',
    40: '#31B370',
    50: '#27935B',
    60: '#27935B',
    70: '#115130',
    80: '#08361E',
    90: '#031C0E',
  },
  navy: '#16276D',
  green: '#27935B',
  orange: '#DB5719',
  blue: '#26A9DC',
  purple: '#590649',
  yellow: '#F0AC28',
  'light-blue': '#D3EBFD',
};

module.exports = {
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./app.vue",
  ],
  theme: {
    extend: {
      screens: {
        xs: '0px',
        mobile: '0px',
        sm: '600px',
        tablet: '600px',
        md: '905px',
        lg: '1240px',
        desktop: '1240px',
        xl: '1440px',
      },
      colors: {
        ...colors,
        content: {
          primary: colors.neutral[90],
          secondary: colors.neutral[50],
          icon: colors.neutral[40],
          inverted: colors.white,
          brand: colors.brand[60],
          negative: colors.negative[50],
          positive: colors.positive[50],
        },
        border: {
          primary: colors.neutral[20],
          subtle: colors.neutral[10],
        },
        bg: {
          screen: colors.white,
          surface: colors.white,
          brand: colors.brand[60],
          light: colors['light-blue'],
        },
        action: {
          primary: {
            label: colors.white,
            bg: {
              default: colors.brand[60],
              hover: colors.brand[50],
              pressed: colors.brand[70],
              disabled: colors.brand[30],
            },
          },
          secondary: {
            label: {
              default: colors.neutral[90],
              disabled: colors.neutral[30],
            },
            icon: {
              default: colors.neutral[90],
              disabled: colors.neutral[30],
            },
            bg: {
              default: colors.white,
              hover: colors.neutral[10],
              pressed: colors.neutral[20],
            },
            border: {
              default: colors.neutral[20],
              disabled: colors.neutral[10],
            },
          },
          link: {
            default: colors.brand[60],
            hover: colors.brand[50],
            pressed: colors.brand[70],
          },
        },
        interaction: {
          positive: {
            icon: colors.positive[50],
            title: colors.positive[80],
            description: colors.positive[70],
            bg: colors.positive[10],
            link: {
              default: colors.positive[60],
              hover: colors.positive[50],
              pressed: colors.positive[70],
            }
          },
          warning: {
            icon: colors.warning[50],
            title: colors.warning[80],
            description: colors.warning[70],
            bg: colors.warning[10],
            link: {
              default: colors.warning[60],
              hover: colors.warning[50],
              pressed: colors.warning[70],
            }
          },
          negative: {
            icon: colors.negative[50],
            title: colors.negative[80],
            description: colors.negative[70],
            bg: colors.negative[10],
            link: {
              default: colors.negative[60],
              hover: colors.negative[50],
              pressed: colors.negative[70],
            }
          },
          info: {
            icon: colors.brand[50],
            title: colors.brand[80],
            description: colors.brand[70],
            bg: colors.brand[10],
            link: {
              default: colors.brand[60],
              hover: colors.brand[50],
              pressed: colors.brand[70],
            }
          },
        },
        form: {
          icon: colors.neutral[40],
          'icon-inverted': colors.white,
          bg: {
            default: colors.white,
            disabled: colors.neutral[10],
            active: colors.brand[60],
          },
          border: {
            default: colors.neutral[20],
            disabled: colors.neutral[10],
            active: colors.brand[30],
          },
          placeholder: colors.neutral[40],
          helper: colors.neutral[50],
          label: colors.neutral[70],
        },
        'non-contextual': {
          overlay: colors['neutral-alpha'][80],
          avatar: {
            bg: colors.neutral[40]
          },
          badge: {
            default: {
              label: colors.neutral[60],
              bg: colors.neutral[10]
            },
            negative: {
              label: colors.negative[60],
              bg: colors.negative[10]
            },
            warning: {
              label: colors.warning[60],
              bg: colors.warning[10]
            },
            positive: {
              label: colors.positive[60],
              bg: colors.positive[10]
            },
            info: {
              label: colors.brand[60],
              bg: colors.brand[10]
            },
          },
          tag: {
            icon: colors.brand[60],
            label: colors.neutral[90],
            border: colors.neutral[20],
            bg: colors.white,
          },
        },
      },
      spacing: {
        4: '4pt',
        6: '6pt',
        8: '8pt',
        10: '10pt',
        12: '12pt',
        16: '16pt',
        20: '20pt',
        24: '24pt',
        32: '32pt',
        40: '40pt',
        48: '48pt',
        64: '64pt',
        96: '96pt',
        120: '120pt'
      },
      fontFamily: {
        inter: ['Inter', 'sans-serif'],
        anuphan: ['Anuphan', 'sans-serif'],
      },
      fontSize: {
        xs: '14pt',
        sm: '16pt',
        base: '18pt',
        lg: '20pt',
        xl: '22pt',
        '2xl': '24pt',
        '3xl': '26pt',
        '4xl': '28pt',
        '5xl': '32pt',
        '6xl': '48pt',
      },
      fontWeight: {
        normal: 400,
        semibold: 600,
        bold: 700,
      }
      // height: {
      //   'fit': 'fit-content',
      //   '18': '4.5rem',
      // },
      // width: {
      //   'fit': 'fit-content',
      //   '18': '4.5rem',
      // },
      // zIndex: {
      //   '-1': '-1',
      // }
    }
  },
  plugins: [],
}