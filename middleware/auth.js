import { useAuthStore } from '@/stores/auth'

export default defineNuxtRouteMiddleware(async (to, from) => {
  const i18nRouter = useI18nRouter()
  const authStore = useAuthStore()

  if (authStore.isLoggedIn === false) {
    return i18nRouter.push('/login');
  }
})
