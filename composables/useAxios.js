import axios from 'axios'
import { useAuthStore } from '@/stores/auth'

export const useAxios = () => {
  const instance = axios.create({
    baseURL: process.env.API_BASE_URL,
    headers: {}
  })

  const authStore = useAuthStore()
  const i18nRounter = useI18nRouter()
  const nuxt = useNuxtApp()
  
  let locale = '*'
  if ('__VUE_I18N__' in nuxt.vueApp) {
    locale = nuxt.vueApp.__VUE_I18N__.global.localeProperties.value.iso
  }

  instance.interceptors.request.use((config) => {
    if (authStore.isLoggedIn) {
      config.headers.set('Authorization', `Bearer ${authStore.token}`)
    }
    if (locale) {
      config.headers.set('Accept-Language', locale)
    }

    return config
  })

  instance.interceptors.response.use((response) => {
    if (response.status === 201) {
      authStore.logOut()
      i18nRounter.push('/login')

    } else if (response.headers.has('Authorization')) {
      const token = response.headers.get('Authorization')
      console.log('Updated JWT token', token)
      authStore.updateToken(token)
    }
    return response
  })

  return instance
}
