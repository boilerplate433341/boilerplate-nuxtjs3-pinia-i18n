export const useI18nRouter = () => {
  return {
    push: (to) => {
      const localePath = useLocalePath()
      const router = useRouter()
    
      router.push(localePath(to))
    }
  }
}
