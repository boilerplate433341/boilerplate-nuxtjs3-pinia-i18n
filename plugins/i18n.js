/**
 * i18n plugin to replace the locale param in URI afther switched to another locale
 */

export default defineNuxtPlugin(nuxtApp => {
  nuxtApp.hook('i18n:beforeLocaleSwitch', ({ oldLocale, newLocale /*, initialSetup, context*/ }) => {
    const { $bus } = useNuxtApp()

    // broadcast event thru event bus
    $bus.emit('locale_changing', {
      current: oldLocale,
      to: newLocale
    })
  })

  nuxtApp.hook('i18n:localeSwitched', ({ oldLocale, newLocale }) => {
    const router = useRouter();
    const { $bus } = useNuxtApp()

    // replace locale param in URI
    router.push({
      path: router.currentRoute.value.fullPath.replace(`/${oldLocale}/`, `/${newLocale}/`)
    })

    // broadcast event thru event bus
    $bus.emit('locale_changing', {
      current: oldLocale,
      to: newLocale
    })
  })

})
