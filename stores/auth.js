import { defineStore } from 'pinia'
const tokenCookie = useCookie(
  'token', {
    default: () => ''
  }
)
const userCookie = useCookie(
  'user',
  {
    default: () => {}
  }
)

export const useAuthStore = defineStore('auth', {
  state: () => {
    return {
      user: userCookie,
      token: tokenCookie.value
    }
  },
  getters: {
    isLoggedIn: (state) => state.token !== null && state.token !== '',
    userEmail: (state) => state.user && 'email' in state.user ? state.user.email : '-',
    avatarUrl: (state) => {
      if (state.user && 'avatarUrl' in state.user && state.user.avatarUrl) {
        return state.user.avatarUrl
      } else {
        '/img/avatar-dummy.jpg'
      }
    },
    // TODO:
    isAttendee: (state) => false,
    isOwner: (state) => true,
    isStaff: (state) => false,
  },
  actions: {
    updateUser (user) {
      this.user = user
      userCookie.value = user
    },
    updateToken (token) {
      this.token = token
      tokenCookie.value = token
    },
    logOut () {
      this.user = {}
      userCookie.value = {}

      this.token = ''
      tokenCookie.value = ''
    },
  },
})