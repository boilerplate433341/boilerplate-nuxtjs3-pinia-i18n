import { defineStore } from 'pinia'

const alertType = {
  success: 'success',
  warning: 'warning',
  danger: 'danger',
  info: 'info',
}

export const useAlertStore = defineStore('alert', {
  state: () => {
    return {
      show: false,
      title: '',
      message: '',
      actions: [],  // [{ title: callback }]
      withIcon: true,
      autohide: true,
      autohideDelay: 3, // seconds
      type: '',
    }
  },
  getters: {
    isTypeSuccess: (state) => state.type === alertType.success,
    isTypeWarning: (state) => state.type === alertType.warning,
    isTypeDanger: (state) => state.type === alertType.danger,
    isTypeInfo: (state) => state.type === alertType.info,
    typeIcon (state) {
      switch (state.type) {
        case alertType.success: return '/img/icon-success.svg'
        case alertType.warning: return '/img/icon-warning.svg'
        case alertType.danger: return '/img/icon-danger.svg'
        default: return '/img/icon-info.svg'
      }
    },
    typeClass (state) {
      switch (state.type) {
        case alertType.success: return 'positive'
        case alertType.warning: return 'warning'
        case alertType.danger: return 'danger'
        default: return 'info'
      }
    }
  },
  actions: {
    hideAlert () {
      this.show = false
    },
    showSuccess(title, message = '', actions = [], withIcon = true, autohide = true) {
      nextTick(() => {
        this.hideAlert()

        nextTick(() => {
          this.show = true
          this.title = title
          this.message = message
          this.actions = actions
          this.withIcon = withIcon
          this.autohide = autohide
          this.type = alertType.success
        })
      })
    },
    showWarning(title, message = '', actions = [], withIcon = true, autohide = true) {
      nextTick(() => {
        this.hideAlert()

        nextTick(() => {
          this.show = true
          this.title = title
          this.message = message
          this.actions = actions
          this.withIcon = withIcon
          this.autohide = autohide
          this.type = alertType.warning
        })
      })
    },
    showDanger(title, message = '', actions = [], withIcon = true, autohide = true) {
      nextTick(() => {
        this.hideAlert()

        nextTick(() => {
          this.show = true
          this.title = title
          this.message = message
          this.actions = actions
          this.withIcon = withIcon
          this.autohide = autohide
          this.type = alertType.danger
        })
      })
    },
    showInfo(title, message = '', actions = [], withIcon = true, autohide = true) {
      nextTick(() => {
        this.hideAlert()

        nextTick(() => {
          this.show = true
          this.title = title
          this.message = message
          this.actions = actions
          this.withIcon = withIcon
          this.autohide = autohide
          this.type = alertType.info
        })
      })
    },
  },
})