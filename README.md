# Nuxt3 Composition Boilerplate

Look at the [Nuxt 3 documentation](https://nuxt.com/docs/getting-started/introduction) to learn more.

## Setup

Make sure to install the dependencies:

```bash
# npm
npm install
```

## Development
```bash
# npm
npm run dev
```

## Directory Structure
| Directory | Description | Note |
|---|---|---|
| api | API wrapper | |
| assets | Dynamic assets, e.g. CSS |  |
| componenets | Vue component | Automatically import |
| composable | Composable object that can call anywhere in project |  |
| layout | Page layout some specific page | E.g. anonymous, signed in and staff layout |
| locales | Conains language translation files | Contains th.json & en.json |
| middleware | Middleware to be call before render each page | Middleware with .global runs by default |
| pages | Page |  |
| plugins | Custom Vue plugin | Automatically import, plugin with .client runs in client |
| public | Static assets | E.g. /img, /font |
| server |  |  |
| stores | State management |  |